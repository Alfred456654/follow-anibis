# follow-anibis

Poll an anibis.ch search results page and use a Telegram bot to notify of the new results.

## Setup

* Create a bot with [@BotFather](tg://resolve?domain=botfather)
* Get its `token`
* Create a Telegram group
* Add your newly created bot to the group
* Execute `curl -s -X POST "https://api.telegram.org/bot${BOT_TOKEN}/getUpdates"` to retrieve the Telegram group's `chat_id`
* Create a `config.properties` file using the sample provided, and fill in all 3 fields

## Usage

`follow-anibis.sh` works without any command line option.

The only optional parameter is to provide one alternate `config.properties` file.
