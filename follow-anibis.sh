#!/bin/bash
CONFIG_FILE="config.properties"
USER_AGENT="Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0"
REGEX_FLOAT="^[+-]?[0-9]+([.][0-9]*)?$"
FILTER_OUT_WORDS="chambre colocation"

function display_price {
    if [ $# -ne 1 ]; then
        echo "ERROR: Usage: $0 price_to_display" > /dev/stderr
        return
    fi
    PRICE="$1"
    if ! [[ ${PRICE%\.} =~ ${REGEX_FLOAT} ]]; then
        echo "ERROR: '${PRICE}' doesn't look like a number" > /dev/stderr
        echo "${PRICE}"
        return
    fi
    INT_P="${PRICE%%\.*}"
    DEC_P=""
    echo "${PRICE}" | grep -q '\.' && DEC_P="${PRICE##*\.}"
    INT_F=$(printf "%'d" "${INT_P}" | tr \, \')
    if [ -z "${DEC_P}" ]; then
        echo "${INT_F}"
    else
        echo "${INT_F}.${DEC_P}"
    fi
}

function go_in_this_dir {
    THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    cd "${THIS_DIR}" || exit
}

function check_config {
	err_flag=0
	if [ ! -f "${CONFIG_FILE}" ]; then
		echo "ERROR: config file '${CONFIG_FILE}' not found" > /dev/stderr
		err_flag=1
	else
        # shellcheck source=config.properties
		source "${CONFIG_FILE}"
		if [ -z "${CHAT_ID}" ]; then
			echo "ERROR: CHAT_ID not set in '${CONFIG_FILE}'" > /dev/stderr
			err_flag=1
		fi
		if [ -z "${ANIBIS_URL}" ]; then
			echo "ERROR: ANIBIS_URL not set in '${CONFIG_FILE}'" > /dev/stderr
			err_flag=1
		fi
		if [ -z "${BOT_TOKEN}" ]; then
			echo "ERROR: BOT_TOKEN not set in '${CONFIG_FILE}'" > /dev/stderr
			err_flag=1
		fi
	fi
	if [ "${err_flag}" -ne 0 ]; then
		exit 1
	fi
    CONFIG_FILE_NAME=$(basename "${CONFIG_FILE}")
    LAST_OUTPUT=".last_output_${CONFIG_FILE_NAME}.txt"
	if [ ! -f "${LAST_OUTPUT}" ]; then
		touch "${LAST_OUTPUT}"
	fi
}

function scrape_anibis {
	if [ $# -ne 1 ]; then
		echo "ERROR: Usage: $0 <anibis_search_results_url>" > /dev/stderr
		exit 1
	fi
	REMOVE_WORDS="-e $(echo ${FILTER_OUT_WORDS} | sed -e 's/ / -e /g')"
	ANIBIS_URL="$1"
	wget -qO- --user-agent="${USER_AGENT}" "${ANIBIS_URL}" | \
		grep '<script id="state">' | \
		sed -e 's_.*<script id="state">\([^<]*\).*_\1_g' \
			-e 's/[^{]*\({.*\)/\1/g' | \
		jq -r '.search.listings.items[] | [ .title, .price, .url ] | "<b>CHF \(.[1]).-</b> | <a href=\"https://www.anibis.ch\(.[2])\">\(.[0])</a>"' | \
		sed -e 's_\(https://www.anibis.ch[^?]*\)?[^"]*_\1_g' | \
		sort | while read -r line; do
            PRICE=$(echo "$line" | sed -e 's/^<b>CHF \([0-9\.]*\)-<\/b>.*/\1/g')
            if [ -n "${PRICE}" ]; then
                PRICE=$(display_price "${PRICE}")
                echo "$line" | sed -e "s/^<b>CHF [^-]*-<\/b>\(.*\)/<b>CHF ${PRICE}-<\/b>\1/g"
            else
                echo "$line"
            fi
        done | grep -vi ${REMOVE_WORDS}
}

function send_telegram_message {
	if [ $# -ne 3 ]; then
		echo "ERROR: Usage: $0 'telegram_bot_token' 'telegram_chat_id' 'message to send'" > /dev/stderr
		exit 1
	fi
	BOT_TOKEN="$1"
	CHAT_ID="$2"
	MESSAGE="$3"
	curl -s -X POST \
		"https://api.telegram.org/bot${BOT_TOKEN}/sendMessage" \
		-d chat_id="${CHAT_ID}" \
		-d text="${MESSAGE}" \
		-d disable_web_page_preview="True" \
		-d parse_mode="HTML"
}

go_in_this_dir
if [ $# -gt 1 ]; then
    echo "ERROR: Usage: $0 [alternate_config_file]" > /dev/stderr
    exit 1
fi
if [ $# -eq 1 ]; then
    CONFIG_FILE="$1"
fi
check_config
mv -f "${LAST_OUTPUT}" "${LAST_OUTPUT}.1"
scrape_anibis "${ANIBIS_URL}" > "${LAST_OUTPUT}"
TELEGRAM_MESSAGE=$(comm -13 "${LAST_OUTPUT}.1" "${LAST_OUTPUT}")
if [ -n "${TELEGRAM_MESSAGE}" ]; then
    send_telegram_message "${BOT_TOKEN}" "${CHAT_ID}" "${TELEGRAM_MESSAGE}"
fi
rm -f "${LAST_OUTPUT}.1"
